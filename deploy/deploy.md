# Настройка окружения

## Настройка redis
В файле redis.conf настроить:

1. requirepass <password>;
2. dir <path to redis db>.
Старт redis `redis-server /usr/local/etc/redis.conf > /dev/null &`.

## Настройка nginx

1. Создать группу webdev:

2. Создать пользователя webuser и добавить его в группу:
```
useradd webuser
passwd webuser
usermod -a -G webdev webuser
```
3. Создание директорий:
```
sudo mkdir -p /var/www/UrlShorter/
cd /var/www
sudo chmod -R 755 UrlShorter/
sudo chown -R webuser:webdev UrlShorter/

sudo touch /var/log/nginx.access.log
sudo chown webuser:webdev /var/log/nginx.access.log

sudo touch /var/log/nginx.error.log
sudo chown webuser:webdev /var/log/nginx.error.log
```
Конфигурация nginx:
```
user            webuser     webdev;

pid             /var/run/nginx.pid;

error_log       /var/log/nginx.error.log  info;

worker_processes    auto;

events {
    worker_connections      256;
}

http {
    log_format      main    '$remote_addr - $remote_user [$time_local] '
                            '"$request" $status $bytes_sent "$http_referer"';

    server {
        access_log      /var/log/nginx.access.log  main;

        include         mime.types;


        location = / {
            index       /index.html;
        }

        location / {
            proxy_pass         http://127.0.0.1:8080/;
            proxy_redirect     off;

            proxy_set_header   Host         $host;
            proxy_set_header   X-Real-IP    $remote_addr;
        }

        location ~* \.html {
            root    /var/www/UrlShorter/html;
        }

        location /public {
            alias    /var/www/UrlShorter;
        }

        location ~ /favicon.ico$ {
            alias   /var/www/UrlShorter/favicon.ico;
        }
    }
}
```
# Настройка idea
Артефакт url-shorter.war деплоится на адрес /url-shorter, ресурсы из **resources** деплоятся на /url-shorter/public.