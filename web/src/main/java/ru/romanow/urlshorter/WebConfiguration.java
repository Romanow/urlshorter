package ru.romanow.urlshorter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by ronin on 19.11.15
 */
@Configuration
@ComponentScan(basePackages = "ru.romanow.urlshorter",
        includeFilters = { @ComponentScan.Filter(RestController.class),
                           @ComponentScan.Filter(ControllerAdvice.class) })
public class WebConfiguration {

    @Value("${springfox.documentation.swagger.v2.path}")
    private String swaggerUrl;

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping(swaggerUrl).allowedOrigins("*");
            }
        };
    }
}
