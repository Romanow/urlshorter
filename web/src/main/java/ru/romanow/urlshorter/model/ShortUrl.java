package ru.romanow.urlshorter.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by ronin on 15.11.15
 */
@ApiModel(description = "Данные о коротком url", parent = RestResponse.class)
public class ShortUrl
        extends RestResponse {

    @ApiModelProperty("Короткий url")
    private String url;

    @ApiModelProperty("Время, когда короткая ссылка будет удалена из базы")
    private String expiredAt;

    public ShortUrl() {}

    public ShortUrl(String url, String expiredAt) {
        this.url = url;
        this.expiredAt = expiredAt;
    }

    public String getUrl() {
        return url;
    }

    public String getExpiredAt() {
        return expiredAt;
    }
}
