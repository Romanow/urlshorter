package ru.romanow.urlshorter.model.enums;

import java.util.concurrent.TimeUnit;

/**
 * Created by ronin on 15.11.15
 */
public enum TimeToLive {
    HOUR(1, TimeUnit.HOURS),
    DAY(1, TimeUnit.DAYS),
    WEEK(7, TimeUnit.DAYS),
    MONTH(30, TimeUnit.DAYS);

    private int duration;
    private TimeUnit timeUnit;

    TimeToLive(int duration, TimeUnit timeUnit) {
        this.duration = duration;
        this.timeUnit = timeUnit;
    }

    public int getDuration() {
        return duration;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }
}
