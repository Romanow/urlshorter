package ru.romanow.urlshorter.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by ronin on 16.11.15
 */
@ApiModel(description = "Ошибка", parent = RestResponse.class)
public class ErrorResponse
        extends RestResponse {

    @ApiModelProperty("Описание ошибки")
    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
