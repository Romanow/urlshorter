package ru.romanow.urlshorter.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;
import ru.romanow.urlshorter.model.enums.TimeToLive;

import javax.validation.constraints.NotNull;

/**
 * Created by ronin on 15.11.15
 */
@ApiModel(description = "Url и необходимое время жизни")
public class UrlInfo {

    @NotEmpty
    @URL
    @ApiModelProperty("Url для получения короткой ссылки")
    private String url;

    @NotNull
    @ApiModelProperty(value = "Время жизни в базе данных", allowableValues = "HOUR,DAY,WEEK,MONTH")
    private TimeToLive timeToLive;

    public String getUrl() {
        return url;
    }

    public UrlInfo setUrl(String url) {
        this.url = url;
        return this;
    }

    public TimeToLive getTimeToLive() {
        return timeToLive;
    }

    public UrlInfo setTimeToLive(TimeToLive timeToLive) {
        this.timeToLive = timeToLive;
        return this;
    }
}
