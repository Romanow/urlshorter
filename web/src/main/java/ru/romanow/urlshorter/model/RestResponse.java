package ru.romanow.urlshorter.model;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * Created by ronin on 16.11.15
 */
@ApiModel(description = "Базовый класс для ответов rest")
public class RestResponse
        implements Serializable {}
