package ru.romanow.urlshorter.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

/**
 * Created by ronin on 16.11.15
 */
@Repository
public class UrlRepositoryImpl
        implements UrlRepository {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public String get(String key) {
        return stringRedisTemplate.<String, String>opsForValue().get(key);
    }

    @Override
    public void put(String key, String value) {
        stringRedisTemplate.<String, String>opsForValue().set(key, value);
    }

    @Override
    public void put(String key, String value, int duration, TimeUnit timeUnit) {
        stringRedisTemplate.<String, String>opsForValue().set(key, value, duration, timeUnit);
    }
}
