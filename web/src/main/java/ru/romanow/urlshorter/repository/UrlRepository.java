package ru.romanow.urlshorter.repository;

import java.util.concurrent.TimeUnit;

/**
 * Created by ronin on 16.11.15
 */
public interface UrlRepository {
    String get(String key);

    void put(String key, String value);

    void put(String key, String value, int duration, TimeUnit timeUnit);
}