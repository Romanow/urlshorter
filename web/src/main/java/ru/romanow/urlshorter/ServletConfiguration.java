package ru.romanow.urlshorter;

import com.google.common.collect.Sets;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionTrackingMode;

/**
 * Created by ronin on 09.11.15
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = "ru.romanow.urlshorter",
        includeFilters = @ComponentScan.Filter(Configuration.class))
public class ServletConfiguration
        extends SpringBootServletInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        this.setRegisterErrorPageFilter(false);
        super.onStartup(servletContext);

        servletContext.setSessionTrackingModes(Sets.newHashSet(SessionTrackingMode.COOKIE));
    }
}
