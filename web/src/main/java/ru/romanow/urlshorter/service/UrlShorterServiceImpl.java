package ru.romanow.urlshorter.service;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import ru.romanow.urlshorter.exception.UrlNotFoundException;
import ru.romanow.urlshorter.model.enums.TimeToLive;
import ru.romanow.urlshorter.repository.UrlRepository;

import java.net.MalformedURLException;

/**
 * Created by ronin on 16.11.15
 */
@Service
public class UrlShorterServiceImpl
        implements UrlShorterService {

    private static final Logger logger = LoggerFactory.getLogger(UrlShorterService.class);

    @Autowired
    private UrlRepository urlRepository;

    @Override
    public String shortUrl(String baseUrl, String fullUrl, TimeToLive timeToLive)
            throws MalformedURLException {

        if (!UrlValidator.getInstance().isValid(fullUrl)) {
            throw new MalformedURLException();
        }

        String key = DigestUtils.md5DigestAsHex(fullUrl.getBytes()).substring(0, 8);
        urlRepository.put(key, fullUrl, timeToLive.getDuration(), timeToLive.getTimeUnit());
        return baseUrl + "/" + key;
    }

    @Override
    public String fullUrl(String shortUrl) throws UrlNotFoundException {
        String fullUrl = urlRepository.get(shortUrl);
        if (fullUrl == null) {
            throw new UrlNotFoundException();
        }

        return fullUrl;
    }
}
