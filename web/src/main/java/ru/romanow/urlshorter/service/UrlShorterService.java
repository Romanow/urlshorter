package ru.romanow.urlshorter.service;

import ru.romanow.urlshorter.exception.UrlNotFoundException;
import ru.romanow.urlshorter.model.enums.TimeToLive;

import java.net.MalformedURLException;

/**
 * Created by ronin on 16.11.15
 */
public interface UrlShorterService {
    String shortUrl(String baseUrl, String fullUrl, TimeToLive timeToLive)
            throws MalformedURLException;

    String fullUrl(String shortUrl) throws UrlNotFoundException;
}
