package ru.romanow.urlshorter.service;

import org.springframework.validation.FieldError;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ronin on 01.02.16
 */
public class ValidationDescriptionHelper {
    public static String getFieldsError(final List<FieldError> errors) {
        return errors.stream()
                     .map(err -> "field [" + err.getField() + "] has error: " + err.getDefaultMessage())
                     .collect(Collectors.joining(","));
    }
}
