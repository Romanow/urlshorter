package ru.romanow.urlshorter.exception;

/**
 * Created by ronin on 17.11.15
 */
public class UrlNotFoundException
        extends Exception {}
