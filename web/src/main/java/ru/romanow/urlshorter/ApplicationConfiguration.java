package ru.romanow.urlshorter;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 * Created by ronin on 09.11.15
 */
@Configuration
@ComponentScan(basePackages = "ru.romanow.urlshorter",
        includeFilters = { @ComponentScan.Filter(Service.class),
                           @ComponentScan.Filter(Repository.class) })
public class ApplicationConfiguration {}
