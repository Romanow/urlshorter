package ru.romanow.urlshorter.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.romanow.urlshorter.exception.UrlNotFoundException;
import ru.romanow.urlshorter.model.ErrorResponse;

import javax.validation.ValidationException;
import java.net.MalformedURLException;

/**
 * Created by ronin on 17.11.15
 */
@ControllerAdvice
public class ExceptionController {

    @Value("${service.static.content}")
    private String serviceUrl;

    @ExceptionHandler(MalformedURLException.class)
    public ResponseEntity<ErrorResponse> errorUrl() {
        return new ResponseEntity<>(new ErrorResponse("Некорректный url"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ErrorResponse> validationError(Exception exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UrlNotFoundException.class)
    public String urlNotFound() {
        return "redirect:" + serviceUrl + "/notfound.html";
    }
}