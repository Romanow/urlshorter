package ru.romanow.urlshorter.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.romanow.urlshorter.exception.UrlNotFoundException;
import ru.romanow.urlshorter.model.ErrorResponse;
import ru.romanow.urlshorter.model.ShortUrl;
import ru.romanow.urlshorter.model.UrlInfo;
import ru.romanow.urlshorter.model.enums.TimeToLive;
import ru.romanow.urlshorter.service.UrlShorterService;
import ru.romanow.urlshorter.service.ValidationDescriptionHelper;

import javax.validation.Valid;
import javax.validation.ValidationException;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by ronin on 15.11.15
 */
@Api
@RestController
public class UrlShorterController {
    private static final DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    @Value("${service.url}")
    private String serviceUrl;

    @Autowired
    private UrlShorterService urlShorterService;

    @ApiOperation(value = "Получение короткой ссылки", response = ShortUrl.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Короткий url и время жизни", response = ShortUrl.class),
            @ApiResponse(code = 400, message = "Ошибка валидации объекта или некорректный url", response = ErrorResponse.class)
    })
    @RequestMapping(value = "/short", method = RequestMethod.POST)
    public ResponseEntity shortUrl(@RequestBody @Valid UrlInfo urlInfo, BindingResult result)
            throws MalformedURLException, ValidationException {
        if (result.hasErrors()) {
            throw new ValidationException(ValidationDescriptionHelper.getFieldsError(result.getFieldErrors()));
        }
        TimeToLive timeToLive = urlInfo.getTimeToLive();
        String url = urlShorterService.shortUrl(serviceUrl,
                                                urlInfo.getUrl(),
                                                timeToLive);

        return new ResponseEntity<>(new ShortUrl(url, getExpiredTime(timeToLive)), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Получение полного url по короткой ссылке")
    @ApiResponses(value = {
            @ApiResponse(code = 301, message = "Редирект по полной ссылке"),
    })
    @RequestMapping(value = "/{line}", method = RequestMethod.GET)
    public String getFullUrl(@PathVariable String line)
            throws UrlNotFoundException {
        return "redirect:" + urlShorterService.fullUrl(line);
    }

    private String getExpiredTime(TimeToLive timeToLive) {
        long hours = timeToLive.getTimeUnit().toHours(timeToLive.getDuration());
        return formatter.format(LocalDateTime.now().plusHours(hours));
    }
}
