package ru.romanow.urlshorter.web;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;
import ru.romanow.urlshorter.ApplicationConfiguration;
import ru.romanow.urlshorter.WebConfiguration;
import ru.romanow.urlshorter.model.ShortUrl;
import ru.romanow.urlshorter.model.UrlInfo;
import ru.romanow.urlshorter.model.enums.TimeToLive;
import ru.romanow.urlshorter.repository.UrlRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Created by ronin on 19.11.15
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = {
        ApplicationConfiguration.class, WebConfiguration.class
})
public class UrlShorterControllerTest {
    private static final String FULL_URL = "http://google.ru";
    private static final DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    @Value("${service.url}")
    private String serviceUrl;

    @Autowired
    private UrlRepository urlRepository;

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;
    private Gson gson = new Gson();

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(context).build();
    }

    @Test
    public void testSuccessShortUrl() throws Exception {
        UrlInfo urlInfo = new UrlInfo()
                .setUrl(FULL_URL)
                .setTimeToLive(TimeToLive.WEEK);

        MvcResult result =
                mockMvc.perform(post("/short")
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(gson.toJson(urlInfo)))
                       .andExpect(status().isCreated())
                       .andExpect(jsonPath("$.url").value(startsWith(serviceUrl)))
                       .andReturn();

        ShortUrl shortUrl = gson.fromJson(result.getResponse()
                                                .getContentAsString(), ShortUrl.class);

        LocalDate dateTime = LocalDate.from(formatter.parse(shortUrl.getExpiredAt()));
        LocalDate expectedDateTime = LocalDate.now()
                                              .plusWeeks(1);

        assertTrue(expectedDateTime.isEqual(dateTime));
    }

    @Test
    public void testWrongUrlShortUrl() throws Exception {
        UrlInfo urlInfo = new UrlInfo()
                .setUrl("ya.ru.com")
                .setTimeToLive(TimeToLive.DAY);

        mockMvc.perform(post("/short")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(gson.toJson(urlInfo)))
               .andExpect(status().isBadRequest())
               .andExpect(jsonPath("$.message").value(notNullValue()));
    }

    @Test
    public void testSuccessGetFullUrl() throws Exception {
        String line = "1234567890";
        urlRepository.put(line, FULL_URL, 1, TimeUnit.MINUTES);

        mockMvc.perform(get("/" + line))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl(FULL_URL));
    }

    @Test
    public void testNotFoundGetFullUrl() throws Exception {
        mockMvc.perform(get("/1234567890"))
               .andExpect(status().is3xxRedirection())
               .andExpect(redirectedUrl(serviceUrl + "/notfound.html"));
    }
}