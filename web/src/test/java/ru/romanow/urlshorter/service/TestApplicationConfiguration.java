package ru.romanow.urlshorter.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import ru.romanow.urlshorter.repository.UrlRepository;

import static org.mockito.Mockito.mock;

/**
 * Created by ronin on 19.11.15
 */
@Configuration
@Profile("unit-test")
public class TestApplicationConfiguration {

    @Bean
    public UrlRepository urlRepository() {
        return mock(UrlRepository.class);
    }

    @Bean
    public UrlShorterService urlShorterService() {
        return new UrlShorterServiceImpl();
    }
}
