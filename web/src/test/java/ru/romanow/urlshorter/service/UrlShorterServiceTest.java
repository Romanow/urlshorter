package ru.romanow.urlshorter.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.romanow.urlshorter.exception.UrlNotFoundException;
import ru.romanow.urlshorter.model.enums.TimeToLive;
import ru.romanow.urlshorter.repository.UrlRepository;

import java.net.MalformedURLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by ronin on 19.11.15
 */
@ActiveProfiles("unit-test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestApplicationConfiguration.class)
public class UrlShorterServiceTest {
    private static final String SERVICE_URL = "http://localhost";
    private static final String FULL_URL = "http://www.ya.ru";
    private static final String KEY = "1234567890";

    @Autowired
    private UrlRepository urlRepository;

    @Autowired
    private UrlShorterService urlShorterService;

    @Test
    public void testSuccessShortUrl() throws Exception {
        String shortUrl = urlShorterService.shortUrl(SERVICE_URL, FULL_URL, TimeToLive.DAY);

        assertNotNull(shortUrl);
        assertTrue(shortUrl.startsWith(SERVICE_URL));
    }

    @Test(expected = MalformedURLException.class)
    public void testWrongUrlShortUrl() throws Exception {
        urlShorterService.shortUrl(SERVICE_URL, "ya.ru", TimeToLive.DAY);
    }

    @Test
    public void testSuccessFullUrl() throws Exception {
        when(urlRepository.get(KEY)).thenReturn(FULL_URL);
        String fullUrl = urlShorterService.fullUrl(KEY);
        assertEquals(FULL_URL, fullUrl);
    }

    @Test(expected = UrlNotFoundException.class)
    public void testUrlNotFoundFullUrl() throws Exception {
        when(urlRepository.get(KEY)).thenReturn(null);
        urlShorterService.fullUrl(KEY);
    }
}