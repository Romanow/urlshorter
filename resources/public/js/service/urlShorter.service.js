/**
 * Created by ronin on 15.11.15.
 */

(function() {
    angular
        .module('urlShorter')
        .factory('urlShorterService', urlShorterService);

    function urlShorterService($resource) {
        return $resource('/url-shorter/short');
    }
})();