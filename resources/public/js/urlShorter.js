/**
 * Created by ronin on 15.11.15.
 */

(function() {
    'use strict';

    angular
        .module('urlShorter',
                ['ngResource',
                 'ui.bootstrap',
                 'angular-clipboard']);
})();