/**
 * Created by ronin on 15.11.15.
 */
(function() {
    'use strict';

    angular
        .module('urlShorter')
        .controller('UrlShorterController', UrlShorterController);

    function UrlShorterController(urlShorterService, $log) {
        var vm = this;

        vm.processed = false;
        vm.timeToLive = 'DAY';
        vm.url = '';
        vm.hasError = false;
        vm.message = "";

        vm.convert = function() {
            urlShorterService
                .save({
                          'url': vm.url,
                          'timeToLive': vm.timeToLive
                      }).$promise
                .then(function(urlInfo) {
                    vm.hasError = false;
                    vm.processed = true;
                    vm.shortUrl = urlInfo.url;
                    vm.expiredAt = urlInfo.expiredAt;
                }, function(response) {
                    vm.hasError = true;
                    vm.message = response.data.message;
                });
        }
    }
})();